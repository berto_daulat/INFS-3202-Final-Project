-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2018 at 04:28 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `db_post`
--

CREATE TABLE `db_post` (
  `post_id` int(100) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` date NOT NULL,
  `post_by` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_post`
--

INSERT INTO `db_post` (`post_id`, `post_content`, `post_date`, `post_by`, `user_id`) VALUES
(25, 'lalallaal', '2018-05-22', 'rafirizky', 43),
(26, 'hahaha', '2018-05-22', 'dzakialya', 42);

-- --------------------------------------------------------

--
-- Table structure for table `db_post_bible`
--

CREATE TABLE `db_post_bible` (
  `post_id` int(255) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` date NOT NULL,
  `post_by` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_post_event`
--

CREATE TABLE `db_post_event` (
  `post_id` int(255) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` date NOT NULL,
  `post_by` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_post_quran`
--

CREATE TABLE `db_post_quran` (
  `post_id` int(100) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` date NOT NULL,
  `post_by` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_user`
--

CREATE TABLE `db_user` (
  `user_id` int(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `image` varchar(2000) NOT NULL,
  `date_registered` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_user`
--

INSERT INTO `db_user` (`user_id`, `user_name`, `user_email`, `user_password`, `image`, `date_registered`) VALUES
(39, 'hahahaha', 'hehe@yahoo.com', '$2y$10$291T.rvv0atZ8k0kdoWRq.PdkmJ3aAusDma2.by6FtgVxHBpoajF6', 'images/bad-profile-pic-2.jpeg', '2018-05-17'),
(40, 'amelia1', 'amelia@email.com', '$2y$10$NeNSXaEeztna.HUNlZ.fruZQqcKaPbLh0YbO/qDxW7WNj4ZDYkuSi', 'images/bad-profile-pic-2.jpeg', '2018-05-19'),
(41, 'amelia', 'amelia@email.com', '$2y$10$iKBUy3cOsM4O4Ka3GqhFWurJSJGeGtDYXfnsUM0e8os1/jLwBWPqi', 'images/bad-profile-pic-2.jpeg', '2018-05-19'),
(42, 'dzakialya', 'dzaki@gmail.com', '$2y$10$16xklvciWpRtHizPO2SJCuvp1zMq7l8KE048Lz1DQTl/Yoq3imtoa', 'images/Screen Shot 2018-05-10 at 19.34.20.png', '2018-05-20'),
(43, 'rafirizky', 'rafirizky@email.com', '$2y$10$8.7spNuIAdBvoY/2ZcyzpOkfE3XrShZf7VMst9V.qgLNlqQRI5l/O', 'images/Screen Shot 2018-05-22 at 14.21.27.png', '2018-05-22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `db_post`
--
ALTER TABLE `db_post`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `post_by` (`post_by`),
  ADD KEY `user_id_post-to-user_id` (`user_id`);

--
-- Indexes for table `db_post_bible`
--
ALTER TABLE `db_post_bible`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `user_id_bible-to-user_id` (`user_id`);

--
-- Indexes for table `db_post_event`
--
ALTER TABLE `db_post_event`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `user_id_event-to-user_id` (`user_id`);

--
-- Indexes for table `db_post_quran`
--
ALTER TABLE `db_post_quran`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `user_id_quran-to-user_id` (`user_id`);

--
-- Indexes for table `db_user`
--
ALTER TABLE `db_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_name` (`user_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `db_post`
--
ALTER TABLE `db_post`
  MODIFY `post_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `db_post_bible`
--
ALTER TABLE `db_post_bible`
  MODIFY `post_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `db_post_event`
--
ALTER TABLE `db_post_event`
  MODIFY `post_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `db_post_quran`
--
ALTER TABLE `db_post_quran`
  MODIFY `post_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `db_user`
--
ALTER TABLE `db_user`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `db_post`
--
ALTER TABLE `db_post`
  ADD CONSTRAINT `user_id_post-to-user_id` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `db_post_bible`
--
ALTER TABLE `db_post_bible`
  ADD CONSTRAINT `user_id_bible-to-user_id` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `db_post_event`
--
ALTER TABLE `db_post_event`
  ADD CONSTRAINT `user_id_event-to-user_id` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `db_post_quran`
--
ALTER TABLE `db_post_quran`
  ADD CONSTRAINT `user_id_quran-to-user_id` FOREIGN KEY (`user_id`) REFERENCES `db_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
