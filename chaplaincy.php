<?php
  session_start();
  include ('connect.php');
  if(@$_SESSION['user_name']){
    if(@$_GET['action'] == 'logout'){
          session_destroy();
          header('Location: login.php');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Religious App</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">

<!-- Bootstrap core CSS -->
<link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="https://v4-alpha.getbootstrap.com/examples/carousel/carousel.css" rel="stylesheet">
  <style>
  #map{
  	height: 700px;
  }
  </style>
</head>
<body>
	<?php include('header.php'); ?>
	<div id="map"></div>
	<script>
		function initMap(){
			var chaplaincy = {lat: -27.496167, lng: 153.015191};
			var qut = {lat: -27.4774, lng: 153.0284};
			var ministries = {lat: -27.5053, lng: 153.0398};
			var options = {
				zoom: 17.5,
				center: chaplaincy,
				disableDefaultUI: false,
				scrollwheel: true,
				draggable: true,
				mapTypeId: google.maps.MapTypeId.HYBRID,
				maxZoom: 25,
				minZoom: 9
			}

			//New Map
			var map = new google.maps.Map(document.getElementById('map'), options);
		
		addMarker({
			coordinate: chaplaincy,
			content: '<h5>Multi-faith Chaplaincy</h5><h5>(Building 38), UQ</h5><p>St Lucia QLD 4067</p><p>Australia</p><a href="https://goo.gl/u4mN4i">View on Google Map</a>'
		});

		addMarker({
			coordinate: qut,
			content: '<h5>QUT Chaplaincy</h5><h5>Gardens Point</h5><p>Room S219, Level 2</p><p>Australia</p><a href="https://goo.gl/BH9hM4">View on Google Map</a>'
		});

		addMarker({
			coordinate: ministries,
			content: '<h5>Multifaith Academy for</h5><h5>Chaplaincy Ministries</h5><p>54 Marquis St, Greenslopes QLD 4120</p><p>Australia</p><a href="https://goo.gl/ej5CEQ">View on Google Map</a>'
		});

		function addMarker(prop){
			var marker = new google.maps.Marker({
				position: prop.coordinate,
				map: map
			});

			// Check content
			if(prop.content){
				var infoWindow = new google.maps.InfoWindow({
				content: prop.content
			});

			marker.addListener('click', function(){
				infoWindow.open(map, marker);
			});
			}
		}
		}	

	</script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-Gxmr0hkk9ovYFlAacOyGrkzg6yLtzos&callback=initMap">
	</script>
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
</body>
</html>

<?php
}else{
    echo 'You have logged out';
  }
?>
