<?php
  session_start();
  include ('connect.php');
  include('header.php');
  if(@$_SESSION['user_name']){
    if(@$_GET['action'] == 'logout'){
          session_destroy();
          header('Location: login.php');
  }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://v4-alpha.getbootstrap.com/examples/carousel/favicon.ico">

    <title>Religious App</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">

    <!-- Bootstrap core CSS -->
    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://v4-alpha.getbootstrap.com/examples/carousel/carousel.css" rel="stylesheet">
  </head>
  <body>

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="first-slide" src="peace_people.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block">
              <h1>Public Forum</h1>
              <p>Umum</p>
              <p><a class="btn btn-lg btn-primary" href="public_forum.php" role="button">Enter Forum</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="second-slide" src="diversity.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block">
              <h1>Event Forum</h1>
              <p>Event</p>
              <p><a class="btn btn-lg btn-primary" href="event_forum.php" role="button">Enter Forum</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="third-slide" src="quran.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block">
              <h1>Quran Forum</h1>
              <p>Quran</p>
              <p><a class="btn btn-lg btn-primary" href="quran_forum.php" role="button">Enter Forum</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="fourth-slide" src="bible.jpg" alt="Fourth slide">
          <div class="container">
            <div class="carousel-caption d-none d-md-block">
              <h1>Bible Forum</h1>
              <p>Bible</p>
              <p><a class="btn btn-lg btn-primary" href="bible_forum.php" role="button">Enter Forum</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">


      <!-- FOOTER -->
      <center>
      <footer>
        <p>&copy; 2017 Religious App, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>
      </center>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
  </body>
</html>

<?php
}else{
    echo 'You have logged out';
  }
?>
