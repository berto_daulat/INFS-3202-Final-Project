<?php
	session_start();
	include ('connect.php');
	if(@$_SESSION["user_name"]){
		if(@$_GET['action'] == "logout"){
		session_destroy();
		header("Location: login.php");
	}
?>
<html>
<head>
	<title>Religious App</title>
	<br>
	<link href="https://bootswatch.com/4/pulse/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
	  <meta charset="utf-8">
  	  <meta name="viewport" content="width=device-width, initial-scale=1">
  	  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">

		<!-- Bootstrap core CSS -->
		<link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="https://v4-alpha.getbootstrap.com/examples/carousel/carousel.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
    <style>
    	.alert {
    		width: 500px;

    	}
    </style>

</head>

<body>
	<?php include("header.php"); ?>
	<?php 
		$curr_pass = @$_POST['curr_pass'];
		$new_pass = @$_POST['new_pass'];
		$re_pass = @$_POST['re_pass'];

		if(isset($_POST['change_pass'])){
			$sql = "SELECT * FROM db_user WHERE user_name = '".$_SESSION['user_name']."'";
			$check = mysqli_query($connect, $sql);
			$rows = mysqli_num_rows($check);
			while($row = mysqli_fetch_assoc($check)){
				$get_pass = $row['user_password'];
			}
			if(password_verify($curr_pass, $get_pass)) {
				if($re_pass == $new_pass){
					$verified_new_pass = password_hash($new_pass, PASSWORD_BCRYPT);
					$sql1 = "UPDATE db_user SET user_password = '".$verified_new_pass."' WHERE user_name = '".$_SESSION['user_name']."'";
					if($query = mysqli_query($connect, $sql1)){
						?>
						<div class="alert alert-success fade show mx-auto" role="alert">
							<center><strong>Password Changed!</strong></center>
						</div>
						<?php
						header('change_password.php');
					}
				}else{
					?>
					<div class="alert alert-danger fade show mx-auto" role="alert">
							<center><strong>New Password</strong> do not match</center>
					</div>
					<?php 
				}
			}else{
				?>
				<div class="alert alert-danger fade show mx-auto" role="alert">
					<center>Your <strong>Current Password</strong> do not match with our database</center>
				</div>
				<?php
			}
		}
		if(isset($_POST['cancel_change'])){
			header("Location: account.php");
			} 
		?>
	<br><br><br>
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
				<header class="card-header">
					<h4 class="card-title mt-2">Change Password</h4>
				</header>
				<article class="card-body">
					<form method="POST" action="change_password.php">
						<div class="form-group">   
		  					<input type="password" name="curr_pass" class="form-control" placeholder="Current Password">
		  				</div>
		  				<div class="form-group">   
		  					<input type="password" name="new_pass" class="form-control" placeholder="New Password">
		  				</div>
						<div class="form-group">   
		  					<input type="password" name="re_pass" class="form-control" placeholder="Retype New Password">
		  				</div>
						<button type='submit' name='change_pass' class="btn btn-outline-primary btn-block">Change</button>
						<button type='submit' name='cancel_change' class="btn btn-outline-danger btn-block">Cancel</button>
					</form>
				</article>
			</div>
		</div>
	</div>
	</center></form>
</body>
</html>

<?php
}else{
		echo "You must be logged in";
	}
?>