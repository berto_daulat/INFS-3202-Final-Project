<?php
	session_start();
	include ('connect.php');
	if(@$_SESSION["user_name"]){
		if(@$_GET['action'] == "logout"){
		session_destroy();
		header("Location: login.php");
	}
?>
<html>
<head>

	<title>Religious App</title>
	<link href="https://bootswatch.com/4/pulse/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://v4-alpha.getbootstrap.com/examples/carousel/favicon.ico">

    <title>Carousel Template for Bootstrap</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">

    <!-- Bootstrap core CSS -->
    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://v4-alpha.getbootstrap.com/examples/carousel/carousel.css" rel="stylesheet">
</head>

<body>
	<?php 
		include('header.php');
		$sql = "DELETE FROM db_user WHERE user_name = '".$_SESSION["user_name"]."'";
				if (isset($_POST['delete_account'])){
					if (mysqli_query($connect, $sql)){
						header("Location: register.php");
					}
					}
					if(isset($_POST['cancel_delete'])){
						header("Location: account.php");
					}
	?>

	<div class="row justify-content-center mt-5 mx-auto">
		<div class="col-md-6">
			<div class="card">
			<header class="card-header">
				<h4 class="card-title mt-2"><center><strong>Delete Account</strong></center></h4>
			</header>
			<article class="card-body">
				<form action='delete_account.php' method='POST'>
				<center>
					Are you sure you want to delete your account?<br><br>
					<button type='submit' class="btn btn-success" name='delete_account'> Yes </button>
					<button type='submit' class="btn btn-danger" name='cancel_delete'> No!</button>
				</center>
				</form>
			</article>
			</div>
		</div>
	</div>
</body>
</html>

<?php
}else{
		echo "You must be logged in";
	}
?>