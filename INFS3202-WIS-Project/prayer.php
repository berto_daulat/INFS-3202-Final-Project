<?php
  session_start();
  include ('connect.php');
  if(@$_SESSION['user_name']){
    if(@$_GET['action'] == 'logout'){
          session_destroy();
          header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="https://v4-alpha.getbootstrap.com/examples/carousel/favicon.ico">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <title>Prayer Time</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Bootstrap core CSS -->
  <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>

</head>
<style>
  .w3-display-container {
    width: auto;
    height: auto;
  }
</style>
<body>
   <?php include('header.php'); ?>
  <!-- Header -->
  <img class="w3-image" src="mesjid.jpg" alt="Me">
  <div class="w3-display-middle w3-padding-large w3-border w3-wide w3-center">
    <h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">PRAYER</h1>
    <h3 class="w3-hide-medium w3-hide-small">TIMES</h3>
    <br>
      <div class="container">
        <div class="col-md-4 offset-md-4">
          <table class="table table-light" align="w3-center">
            <thead class="thead-dark">
            <tr>
              <th>Prayers</th>
              <th>Times</th>
              <?php
              //TODO 
              $url = 'http://muslimsalat.com/Brisbane/daily.json?key=da0462890c8de376dcb0127e52fa4f74';
              $content = file_get_contents($url);
              $array = json_decode($content, true);
              $country = $array["country"];
              $fajr = $array['items']['0']['fajr'];
              $dhuhr = $array['items']['0']['dhuhr'];
              $asr = $array['items']['0']['asr'];
              $maghrib = $array['items']['0']['maghrib'];
              $isha = $array['items']['0']['isha'];
              echo '<tr>';
              echo '<td>'.'Today In'.'</td>'.'<td>'.$country.'</td>';
              echo '</tr>';
              echo '<tr>';
              echo '<td>'.'Fajr'.'</td>'.'<td>'.$fajr.'</td>';
              echo '</tr>';
              echo '<tr>';
              echo '<td>'.'Dhuhr'.'</td>'.'<td>'.$dhuhr.'</td>';
              echo '</tr>';
              echo '<tr>';
              echo '<td>'.'Asr'.'</td>'.'<td>'.$asr.'</td>';
              echo '</tr>';
              echo '<tr>';
              echo '<td>'.'Maghrib'.'</td>'.'<td>'.$maghrib.'</td>';
              echo '</tr>';
              echo '<tr>';
              echo '<td>'.'Isha'.'</td>'.'<td>'.$isha.'</td>';
              echo '</tr>';
              
            ?>
            </tr>
          </table>
        </thead>
        </div>
      </div>
  </div>

</header>

</body>
</html>
<?php
}else{
    echo 'You have logged out';
  }
?>